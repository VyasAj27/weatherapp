package com.mobiquity.weatherapp.addLocation

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.mobiquity.weatherapp.locations.LocationListViewModel
import com.mobiquity.weatherapp.locations.LocationRepository
import com.mobiquity.weatherapp.models.Location
import com.mobiquity.weatherapp.utils.Dependencies
import com.mobiquity.weatherapp.utils.getOrAwaitValue
import org.junit.Before
import org.junit.Test

import org.junit.Rule
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@Config(sdk = [Build.VERSION_CODES.O_MR1])
@RunWith(AndroidJUnit4::class)
class AddLocationViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var locationRepository: LocationRepository
    private lateinit var addLocationViewModel: AddLocationViewModel
    private lateinit var locationListViewModel: LocationListViewModel

    @Before
    fun setup() {
        locationRepository =
            Dependencies.getLocationRepository(ApplicationProvider.getApplicationContext(), true)

        locationListViewModel =
            LocationListViewModel(ApplicationProvider.getApplicationContext(), locationRepository)

        addLocationViewModel =
            AddLocationViewModel(ApplicationProvider.getApplicationContext(), locationRepository)
    }

    @Test
    fun new_insertLocation() {

        val dummyLocation = Location(name = "Vadodara")

        addLocationViewModel.insertLocation(dummyLocation)

        val list = locationListViewModel.locationList.getOrAwaitValue()

        //assertThat(list[0].name).contains(dummyLocation.name)
    }
}