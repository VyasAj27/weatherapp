package com.mobiquity.weatherapp.models

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

/**
 * This class models Location object.
 * Usually, this will be used to store City details
 * and its relevant weather data.
 */
@Parcelize
@Entity(tableName = "location")
data class Location(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int = 0,
    @ColumnInfo(name = "name")
    var name: String = "",
    @ColumnInfo(name = "dt")
    var date: Long = 0,
    @ColumnInfo(name = "main")
    var main: String = "",
    @ColumnInfo(name = "description")
    var description: String = "",
    @ColumnInfo(name = "temp")
    var temp: Double = 0.0,
    @ColumnInfo(name = "feels_like")
    var feelsLike: Double = 0.0,
    @ColumnInfo(name = "temp_min")
    var tempMin: Double = 0.0,
    @ColumnInfo(name = "temp_max")
    var tempMax: Double = 0.0,
    @ColumnInfo(name = "pressure")
    var pressure: Int = 0,
    @ColumnInfo(name = "humidity")
    var humidity: Int = 0,
    @ColumnInfo(name = "wind_speed")
    var windSpeed: Double = 0.0,
    @ColumnInfo(name = "wind_degree")
    var windDegree: Int = 0,
    var locationNameToPrint : String = ""

) : Parcelable