package com.mobiquity.weatherapp.models;

import com.google.gson.annotations.SerializedName;

public class Main{

	@SerializedName("temp")
	private double temp;

	@SerializedName("temp_min")
	private Double tempMin;

	@SerializedName("grnd_level")
	private int grndLevel;

	@SerializedName("temp_kf")
	private double tempKf;

	@SerializedName("humidity")
	private int humidity;

	@SerializedName("pressure")
	private int pressure;

	@SerializedName("sea_level")
	private int seaLevel;

	@SerializedName("feels_like")
	private Double feelsLike;

	@SerializedName("temp_max")
	private double tempMax;

	public double getTemp(){
		return temp;
	}

	public Double getTempMin(){
		return tempMin;
	}

	public int getGrndLevel(){
		return grndLevel;
	}

	public double getTempKf(){
		return tempKf;
	}

	public int getHumidity(){
		return humidity;
	}

	public int getPressure(){
		return pressure;
	}

	public int getSeaLevel(){
		return seaLevel;
	}

	public Double getFeelsLike(){
		return feelsLike;
	}

	public double getTempMax(){
		return tempMax;
	}

	@Override
	public String toString() {
		return "Main{" +
				"temp=" + temp +
				", tempMin=" + tempMin +
				", grndLevel=" + grndLevel +
				", tempKf=" + tempKf +
				", humidity=" + humidity +
				", pressure=" + pressure +
				", seaLevel=" + seaLevel +
				", feelsLike=" + feelsLike +
				", tempMax=" + tempMax +
				'}';
	}
}