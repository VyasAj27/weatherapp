package com.mobiquity.weatherapp.models

data class ForecastDateWise(val date : String, val listOfItems : List<ListItem>)