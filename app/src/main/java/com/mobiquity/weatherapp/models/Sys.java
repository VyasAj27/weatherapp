package com.mobiquity.weatherapp.models;

import com.google.gson.annotations.SerializedName;

public class Sys{

	@SerializedName("pod")
	private String pod;

	public String getPod(){
		return pod;
	}

	@Override
	public String toString() {
		return "Sys{" +
				"pod='" + pod + '\'' +
				'}';
	}
}