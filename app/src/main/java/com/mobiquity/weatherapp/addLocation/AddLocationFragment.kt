package com.mobiquity.weatherapp.addLocation

import android.Manifest
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.net.PlacesClient
import com.mobiquity.weatherapp.R
import com.mobiquity.weatherapp.databinding.FragmentAddLocationBinding
import com.mobiquity.weatherapp.utils.Dependencies
import com.mobiquity.weatherapp.utils.hideKeyboard
import kotlinx.android.synthetic.main.fragment_add_location.*
import java.util.*

class AddLocationFragment : Fragment() {

    private lateinit var binding : FragmentAddLocationBinding
    private var map: GoogleMap? = null
    private var cameraPosition: CameraPosition? = null

    private val addLocationViewModel: AddLocationViewModel by activityViewModels(){
        Dependencies.getAddLocationViewModelFactory(requireActivity().application)
    }
    private lateinit var placesClient: PlacesClient
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private val defaultLocation = LatLng(23.0225, 72.5714)
    private var locationPermissionGranted = false
    private var lastKnownLocation: Location? = null

    companion object {
        private val TAG = AddLocationFragment::class.java.simpleName
        private const val DEFAULT_ZOOM = 9.0f
        private const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
        private const val KEY_CAMERA_POSITION = "camera_position"
        private const val KEY_LOCATION = "location"
    }

    private val callback = OnMapReadyCallback { googleMap ->
        map = googleMap
        googleMap.moveCamera(CameraUpdateFactory
            .newLatLngZoom(defaultLocation, DEFAULT_ZOOM))

        getLocationPermission()
        updateLocationUI()
        getDeviceLocation()

        googleMap?.setOnCameraIdleListener {
            val position: LatLng? = googleMap.cameraPosition?.target
            position?.let {
                try {
                    val geo = Geocoder(
                        requireActivity().applicationContext,
                        Locale.getDefault()
                    )
                    val addresses =
                        geo.getFromLocation(it.latitude, it.longitude ,1)
                    if (addresses.isEmpty()) {
                        tvAddress.text = getString(R.string.msg_waiting_location)
                        pbLoader.visibility = View.VISIBLE
                        ivMarker.visibility = View.GONE
                    } else {
                        if (addresses.size > 0) {
                            var cityName = ""
                            if(!addresses[0].locality.isNullOrEmpty()){
                                cityName = addresses[0].locality+ ", "
                            }
                            else if(!addresses[0].subAdminArea.isNullOrEmpty()){
                                cityName = addresses[0].subAdminArea+ ", "
                            }

                            tvAddress.text =
                                cityName + addresses[0]
                                    .adminArea + ", " + addresses[0].countryName
                        }
                        pbLoader.visibility = View.GONE
                        ivMarker.visibility = View.VISIBLE
                    }
                } catch (e: Exception) {
                    e.printStackTrace() // getFromLocation() may sometimes fail
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        if (savedInstanceState != null) {
            lastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION)
            cameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION)
        }

        binding = FragmentAddLocationBinding.inflate(inflater,container,false)
        init()
        return binding.root
    }

    private fun init() {
        Places.initialize(requireActivity().applicationContext, getString(R.string.google_maps_key))
        placesClient = Places.createClient(requireActivity())
        fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(requireActivity())

        binding.llSave.setOnClickListener {
            val location = com.mobiquity.weatherapp.models.Location(name = tvAddress.text.toString())
            pbLoader.visibility = View.VISIBLE
            ivMarker.visibility = View.GONE
            addLocationViewModel.insertLocation(location)
                .observe(viewLifecycleOwner, Observer { id ->
                    if (id > 0) {
                        hideKeyboard()
                        requireActivity().onBackPressed()
                    }
                    pbLoader.visibility = View.GONE
                    ivMarker.visibility = View.VISIBLE

                })
        }

        binding.tbHeader.setNavigationIcon(R.drawable.ic_back)
        binding.tbHeader.setNavigationOnClickListener { requireActivity().onBackPressed() }
    }

    /**
     * Saves the state of the map when the activity is paused.
     */
    // [START maps_current_place_on_save_instance_state]
    override fun onSaveInstanceState(outState: Bundle) {
        map?.let { map ->
            outState.putParcelable(KEY_CAMERA_POSITION, map.cameraPosition)
            outState.putParcelable(KEY_LOCATION, lastKnownLocation)
        }
        super.onSaveInstanceState(outState)
    }

    /*
    * Request location permission, so that we can get the location of the
    * device. The result of the permission request is handled by a callback,
    * onRequestPermissionsResult.
    */
    private fun getLocationPermission() {
        if (ContextCompat.checkSelfPermission(requireActivity().applicationContext,
                Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED) {
            locationPermissionGranted = true
        } else {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }

    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    private fun getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (locationPermissionGranted) {
                val locationResult = fusedLocationProviderClient.lastLocation
                locationResult.addOnCompleteListener(requireActivity()) { task ->
                    if (task.isSuccessful) {
                        // Set the map's camera position to the current location of the device.
                        lastKnownLocation = task.result
                        if (lastKnownLocation != null) {
                            map?.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                LatLng(lastKnownLocation!!.latitude,
                                    lastKnownLocation!!.longitude), DEFAULT_ZOOM
                            ))
                        }
                    } else {
                        Log.d(TAG, "Current location is null. Using defaults.")
                        Log.e(TAG, "Exception: %s", task.exception)
                        map?.moveCamera(CameraUpdateFactory
                            .newLatLngZoom(defaultLocation, DEFAULT_ZOOM))
                        map?.uiSettings?.isMyLocationButtonEnabled = false
                    }
                }
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }

    /**
     * Handles the result of the request for location permissions.
     */
    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        locationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationPermissionGranted = true
                }
            }
        }
        updateLocationUI()
    }

    private fun updateLocationUI() {
        if (map == null) {
            return
        }
        try {
            if (locationPermissionGranted) {
                map?.isMyLocationEnabled = false
                map?.uiSettings?.isMyLocationButtonEnabled = false
            } else {
                map?.isMyLocationEnabled = false
                map?.uiSettings?.isMyLocationButtonEnabled = false
                lastKnownLocation = null
                getLocationPermission()
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }
}