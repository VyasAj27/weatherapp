package com.mobiquity.weatherapp.addLocation

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mobiquity.weatherapp.locations.LocationRepository

class AddLocationViewModelFactory(
    private val application: Application,
    private val locationRepository: LocationRepository
) : ViewModelProvider.AndroidViewModelFactory(application) {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AddLocationViewModel(application, locationRepository) as T
    }
}