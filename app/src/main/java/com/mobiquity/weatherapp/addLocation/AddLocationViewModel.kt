package com.mobiquity.weatherapp.addLocation

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.mobiquity.weatherapp.locations.LocationRepository
import com.mobiquity.weatherapp.models.Location
import kotlinx.coroutines.launch

class AddLocationViewModel(val app: Application, val locationRepository: LocationRepository) :
    AndroidViewModel(app) {

    fun insertLocation(location: Location): LiveData<Long> {

        val liveDataId = MutableLiveData<Long>()
        viewModelScope.launch {
            val id = locationRepository.insertLocation(location)
            liveDataId.value = id
        }

        return liveDataId
    }

}