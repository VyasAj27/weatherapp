package com.mobiquity.weatherapp.help

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.mobiquity.weatherapp.R
import com.mobiquity.weatherapp.databinding.FragmentSettingsBinding
import com.mobiquity.weatherapp.locations.LocationListViewModel
import com.mobiquity.weatherapp.utils.Dependencies

class SettingsFragment : Fragment() {

    private lateinit var binding: FragmentSettingsBinding

    private val locationListViewModel : LocationListViewModel by activityViewModels {
        Dependencies.getLocationsListViewModelFactory(requireActivity().application)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentSettingsBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        binding.tbHeader.setNavigationIcon(R.drawable.ic_back)
        binding.tbHeader.setNavigationOnClickListener { requireActivity().onBackPressed() }
        binding.onDeleteClickListener = View.OnClickListener {
            locationListViewModel.deleteLocations().observe(viewLifecycleOwner, Observer {
                Toast.makeText(requireActivity().applicationContext,"All Clear!", Toast.LENGTH_LONG).show()
            })
        }
    }
}