package com.mobiquity.weatherapp.retrofit

import com.mobiquity.weatherapp.models.LocationCurrentResponse
import com.mobiquity.weatherapp.models.LocationForecastResponse
import com.mobiquity.weatherapp.utils.Constants
import retrofit2.Call
import retrofit2.http.*

@JvmSuppressWildcards
interface ApiInterface {

    @GET(Constants.GET_WEATHER)
    suspend fun executeGetWeather(
        @Query("q") query: String,
        @Query("appid") appId: String
    ): LocationCurrentResponse

    @GET(Constants.GET_WEATHER_FORECAST_AHEAD)
    fun executeGetWeatherForecast(
        @Query("q") query: String,
        @Query("appid") appId: String
    ): Call<LocationForecastResponse>



}