package com.mobiquity.weatherapp.locations

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mobiquity.weatherapp.db.WeatherDao
import com.mobiquity.weatherapp.models.Location
import com.mobiquity.weatherapp.models.LocationCurrentResponse
import com.mobiquity.weatherapp.models.LocationForecastResponse
import com.mobiquity.weatherapp.retrofit.ApiInterface
import com.mobiquity.weatherapp.utils.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class LocationRepository(private val weatherDao: WeatherDao, val apiInterface: ApiInterface) {

    fun fetchLocations(): LiveData<List<Location>> {
        return weatherDao.fetchLocations()
    }

    suspend fun deleteLocation(id: Int): Boolean {
        withContext(Dispatchers.IO) {
            weatherDao.deleteLocation(id);
        }
        return true
    }

    suspend fun deleteLocations(): Boolean {
        withContext(Dispatchers.IO) {
            weatherDao.deleteLocations();
        }
        return true
    }

    suspend fun insertLocation(location: Location): Long {
        return withContext(Dispatchers.IO) {
            val response: LocationCurrentResponse =
                apiInterface.executeGetWeather(location.name, Constants.APP_ID_OPEN_WEATHER)
            //Log.v("insertLocation", "Response: $response")

            try {
                location.date = response.dt.toLong()
                if (response.weather != null && response.weather.isNotEmpty()) {
                    location.main = response.weather[0].main
                    location.description = response.weather[0].main
                }
                location.temp = response.main.temp
                location.feelsLike = response.main.feelsLike
                location.tempMin = response.main.tempMin
                location.tempMax = response.main.tempMax
                location.pressure = response.main.pressure
                location.humidity = response.main.humidity
                location.windSpeed = response.wind.speed
                location.windDegree = response.wind.deg

                weatherDao.insertLocation(location)
            } catch (e: Exception) {
                throw e
            }

        }
    }

    fun getLocationForecast(query: String): LiveData<LocationForecastResponse> {

        val locationForecastResponseLiveData: MutableLiveData<LocationForecastResponse> =
            MutableLiveData()

        val executeForecast =
            apiInterface.executeGetWeatherForecast(query, Constants.APP_ID_OPEN_WEATHER)

        executeForecast.enqueue(object : Callback<LocationForecastResponse> {
            override fun onResponse(
                call: Call<LocationForecastResponse>,
                response: Response<LocationForecastResponse>
            ) {
                if (response.isSuccessful) {
                    locationForecastResponseLiveData.value = response.body()
                }
            }

            override fun onFailure(call: Call<LocationForecastResponse>, t: Throwable) {
                t.printStackTrace()
            }

        })

        return locationForecastResponseLiveData
    }

}