package com.mobiquity.weatherapp.locations

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.mobiquity.weatherapp.models.Location
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class LocationListViewModel(val app: Application, val locationRepository: LocationRepository) :
    AndroidViewModel(app) {

    val locationList : LiveData<List<Location>> = locationRepository.fetchLocations()

    fun deleteLocation(location : Location) : LiveData<Boolean> {

        val liveData = MutableLiveData<Boolean>()
        viewModelScope.launch {
            val done = locationRepository.deleteLocation(location.id)
            liveData.value = done
        }
        return liveData
    }

    fun deleteLocations() : LiveData<Boolean>{

        val liveData = MutableLiveData<Boolean>()
        viewModelScope.launch {
            val done = locationRepository.deleteLocations()
            liveData.value = done
        }
        return liveData
    }

}