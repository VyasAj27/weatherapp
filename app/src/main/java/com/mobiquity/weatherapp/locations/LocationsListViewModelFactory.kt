package com.mobiquity.weatherapp.locations

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class LocationsListViewModelFactory(
    private val application: Application,
    private val locationRepository: LocationRepository
) : ViewModelProvider.AndroidViewModelFactory(application) {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LocationListViewModel(application, locationRepository) as T
    }
}