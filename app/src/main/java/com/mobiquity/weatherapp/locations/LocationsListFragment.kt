package com.mobiquity.weatherapp.locations

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.mobiquity.weatherapp.R
import com.mobiquity.weatherapp.databinding.FragmentLocationsListBinding
import com.mobiquity.weatherapp.models.Location
import com.mobiquity.weatherapp.utils.Constants
import com.mobiquity.weatherapp.utils.Dependencies


class LocationsListFragment : Fragment() {

    private lateinit var binding: FragmentLocationsListBinding

    private val locationListViewModel: LocationListViewModel by activityViewModels {
        Dependencies.getLocationsListViewModelFactory(requireActivity().application)
    }

    private lateinit var adapter: LocationListAdapter

    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentLocationsListBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    private fun init() {

        adapter = LocationListAdapter { position ->
            val bundle = bundleOf(Constants.LOCATION_BUNDLE to adapter.currentList[position])
            navController.navigate(
                R.id.action_locationsListFragment_to_locationDetailsFragment,
                bundle
            )
        }
        binding.locationList.adapter = adapter
        val itemTouchHelper = ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(
            0,
            ItemTouchHelper.LEFT
        ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false;
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                locationListViewModel.deleteLocation(adapter.currentList.get(viewHolder.adapterPosition) as Location).observe(
                    viewLifecycleOwner, Observer {
                        Toast.makeText(requireActivity().applicationContext,"Deleted!",Toast.LENGTH_LONG).show()
                    }
                )
            }

        })
        itemTouchHelper.attachToRecyclerView(binding.locationList)
        binding.locationList.isNestedScrollingEnabled = false

        navController = findNavController()
        subscribeUi()
    }

    private fun subscribeUi() {
        locationListViewModel.locationList.observe(viewLifecycleOwner, Observer { locationList ->
            binding.hasData = !locationList.isNullOrEmpty()
            locationList.forEach { location ->
                var arr = location.name.split(",")
                if (arr.isNotEmpty()) {
                    location.locationNameToPrint = arr[0];
                }
            }
            adapter.submitList(locationList)
        })

        binding.llAdd.setOnClickListener {
            navController.navigate(R.id.action_locationsListFragment_to_addLocationFragment)
        }

        binding.ivHelp.setOnClickListener{
            navController.navigate(R.id.action_locationsListFragment_to_helpFragment)
        }

        binding.ivSettings.setOnClickListener{
            navController.navigate(R.id.action_locationsListFragment_to_settingsFragment)
        }
    }
}