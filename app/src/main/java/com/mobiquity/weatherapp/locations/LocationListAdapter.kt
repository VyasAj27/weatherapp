package com.mobiquity.weatherapp.locations

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mobiquity.weatherapp.R
import com.mobiquity.weatherapp.databinding.ListItemLocationDataBinding
import com.mobiquity.weatherapp.models.Location

class LocationListAdapter(var onClick: (Int) -> Unit) :
    ListAdapter<Any, RecyclerView.ViewHolder>(LocationDataDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return LocationViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.list_item_location_data, parent, false
            ), onClick
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        holder as LocationViewHolder
        holder.bind(getItem(position) as Location)
    }

    class LocationViewHolder(
        val binding: ListItemLocationDataBinding, var onClick: (Int) -> Unit) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(location: Location) {
            with(binding) {
                this.setClickListener {
                    onClick(adapterPosition)
                }
                this.location = location
                executePendingBindings()
            }
        }
    }
}

private class LocationDataDiffCallback : DiffUtil.ItemCallback<Any>() {
    override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
        if (oldItem is Location && newItem is Location) {
            return oldItem.id == newItem.id
        }
        return false
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean {
        if (oldItem is Location && newItem is Location) {
            return oldItem == newItem
        }
        return false
    }

}