package com.mobiquity.weatherapp.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mobiquity.weatherapp.models.Location
import retrofit2.http.DELETE

@Dao
interface WeatherDao {

    @Query("SELECT * from location")
    fun fetchLocations() : LiveData<List<Location>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertLocation(location: Location) : Long

    @Query("DELETE FROM location")
    suspend fun deleteLocations()


    @Query("DELETE FROM location WHERE id = :id")
    suspend fun deleteLocation(id: Int)

}