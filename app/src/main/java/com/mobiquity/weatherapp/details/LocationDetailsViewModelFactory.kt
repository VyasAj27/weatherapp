package com.mobiquity.weatherapp.details

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mobiquity.weatherapp.addLocation.AddLocationViewModel
import com.mobiquity.weatherapp.locations.LocationRepository

class LocationDetailsViewModelFactory(
    private val application: Application,
    private val locationRepository: LocationRepository
) : ViewModelProvider.AndroidViewModelFactory(application) {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LocationDetailsViewModel(application, locationRepository) as T
    }
}