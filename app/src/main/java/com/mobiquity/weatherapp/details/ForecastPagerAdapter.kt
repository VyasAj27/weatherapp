package com.mobiquity.weatherapp.details

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.mobiquity.weatherapp.models.ForecastDateWise
import com.mobiquity.weatherapp.models.ListItem
import com.mobiquity.weatherapp.models.Location

class ForecastPagerAdapter internal constructor(
    fm: FragmentManager,
    val listOfForecastDateWise: ArrayList<ForecastDateWise>,
    val location : Location
) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {

        return LocationForecastDetailFragment(listOfForecastDateWise[position], position, location)
    }

    override fun getCount(): Int {
        return listOfForecastDateWise.size
    }
}