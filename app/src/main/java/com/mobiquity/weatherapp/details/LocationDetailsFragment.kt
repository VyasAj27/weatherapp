package com.mobiquity.weatherapp.details

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.mobiquity.weatherapp.R
import com.mobiquity.weatherapp.databinding.FragmentLocationDetailsBinding
import com.mobiquity.weatherapp.models.ForecastDateWise
import com.mobiquity.weatherapp.models.ListItem
import com.mobiquity.weatherapp.utils.Constants
import com.mobiquity.weatherapp.utils.Dependencies
import java.util.*
import kotlin.collections.ArrayList

class LocationDetailsFragment : Fragment() {

    private lateinit var binding: FragmentLocationDetailsBinding
    private lateinit var listOfForecast : List<ListItem>
    private lateinit var mapOfDateAvailAndData : MutableMap<String, ArrayList<ListItem>>
    private val dateTimeSDF = Dependencies.provideDateTextSdfTime()
    private val dateSDF = Dependencies.provideDateSdfTime()

    private val locationDetailsViewModel: LocationDetailsViewModel by activityViewModels {
        Dependencies.getLocationDetailsViewModelFactory(requireActivity().application)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentLocationDetailsBinding.inflate(inflater, container, false)
        init()
        loadForecastData()
        return binding.root
    }

    private fun init() {

        mapOfDateAvailAndData = hashMapOf();

        binding.location = arguments?.getParcelable(Constants.LOCATION_BUNDLE)

        binding.tbHeader.setNavigationIcon(R.drawable.ic_back)
        binding.tbHeader.setNavigationOnClickListener { requireActivity().onBackPressed() }
    }

    private fun loadForecastData() {

        binding.loadedForecastData = false

        locationDetailsViewModel.getLocationForecast(binding.location!!.locationNameToPrint)
            .observe(viewLifecycleOwner, Observer { locationForecastResponse ->

                binding.loadedForecastData = true

                Log.v("Forecast", "Forecast Response $locationForecastResponse")

                listOfForecast = locationForecastResponse.list

                if(listOfForecast.isNotEmpty()){

                    listOfForecast.mapIndexed { index, listItem ->
                        val date = Date(listItem.dt * 1000.toLong())
                        val dateKey = dateSDF.format(date)
                        listItem.dateToPrint = dateTimeSDF.format(date)
                        if(mapOfDateAvailAndData.containsKey(dateKey)){
                            var list = mapOfDateAvailAndData[dateKey]
                            if(list == null) mapOfDateAvailAndData[dateKey] = arrayListOf()
                            list?.add(listItem)
                        }
                        else{
                            mapOfDateAvailAndData.put(dateKey, arrayListOf())
                            var list = mapOfDateAvailAndData[dateKey]
                            if(list == null) mapOfDateAvailAndData[dateKey] = arrayListOf()
                            list?.add(listItem)
                        }
                    }

                    initForecastUI()
                }


            })
    }

    private fun initForecastUI(){

        binding.vpInfo.offscreenPageLimit = 1

        val listOfForecastDateWise = arrayListOf<ForecastDateWise>()

        for((key,value) in mapOfDateAvailAndData){
            listOfForecastDateWise.add(ForecastDateWise(key, value))
        }

        listOfForecastDateWise.sortBy {
            forecastDateWise ->
            forecastDateWise.date
        }

        binding.vpInfo.adapter = ForecastPagerAdapter(childFragmentManager, listOfForecastDateWise, binding.location!!)

        binding.tlInfo.setupWithViewPager(binding.vpInfo)
    }
}