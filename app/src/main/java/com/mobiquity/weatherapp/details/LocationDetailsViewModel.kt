package com.mobiquity.weatherapp.details

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.mobiquity.weatherapp.locations.LocationRepository
import com.mobiquity.weatherapp.models.LocationForecastResponse

class LocationDetailsViewModel(val app: Application, val locationRepository: LocationRepository) :
    AndroidViewModel(app) {

    fun getLocationForecast(query : String) : LiveData<LocationForecastResponse> {
        return locationRepository.getLocationForecast(query)
    }
}