package com.mobiquity.weatherapp.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.mobiquity.weatherapp.R
import com.mobiquity.weatherapp.databinding.FragmentForecastDetailBinding
import com.mobiquity.weatherapp.locations.LocationListAdapter
import com.mobiquity.weatherapp.models.ForecastDateWise
import com.mobiquity.weatherapp.models.ListItem
import com.mobiquity.weatherapp.models.Location
import com.mobiquity.weatherapp.utils.Constants
import com.mobiquity.weatherapp.utils.Dependencies
import java.util.*

/**
 * Contains list of time wise data for any given day and if its current day, it shows todays data as well.
 */
class LocationForecastDetailFragment(private val forecastDateWise: ForecastDateWise, val position: Int, val location : Location) : Fragment() {

    private lateinit var binding: FragmentForecastDetailBinding

    private lateinit var adapter: TimeWiseForecastListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentForecastDetailBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    private fun init() {

        adapter = TimeWiseForecastListAdapter()
        binding.forecastList.adapter = adapter
        binding.forecastList.isNestedScrollingEnabled = false

        adapter.submitList(forecastDateWise.listOfItems)

        val dateHere = Dependencies.provideDateSdfTime().parse(forecastDateWise.date)
        val dateHereCal = Calendar.getInstance()
        dateHereCal.time = dateHere!!
        binding.isCurrentDay = Calendar.getInstance().get(Calendar.DATE) == dateHereCal.get(Calendar.DATE)
        binding.location = location;
    }
}