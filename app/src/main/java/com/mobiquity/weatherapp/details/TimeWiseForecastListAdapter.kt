package com.mobiquity.weatherapp.details

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mobiquity.weatherapp.R
import com.mobiquity.weatherapp.databinding.ListItemForecastBinding
import com.mobiquity.weatherapp.databinding.ListItemLocationDataBinding
import com.mobiquity.weatherapp.models.ListItem
import com.mobiquity.weatherapp.models.Location

class TimeWiseForecastListAdapter : ListAdapter<Any, RecyclerView.ViewHolder>(LocationDataDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return TimeWiseForcastViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.list_item_forecast, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        holder as TimeWiseForcastViewHolder
        holder.bind(getItem(position) as ListItem)
    }

    class TimeWiseForcastViewHolder(
        val binding: ListItemForecastBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(listItem : ListItem) {
            with(binding) {
                this.main = listItem.main
                this.wind = listItem.wind
                this.date = listItem.dateToPrint
                executePendingBindings()
            }
        }
    }
}

private class LocationDataDiffCallback : DiffUtil.ItemCallback<Any>() {
    override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
        if (oldItem is Location && newItem is Location) {
            return oldItem.id == newItem.id
        }
        return false
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean {
        if (oldItem is Location && newItem is Location) {
            return oldItem == newItem
        }
        return false
    }

}