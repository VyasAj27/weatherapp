package com.mobiquity.weatherapp.utils

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.mobiquity.weatherapp.addLocation.AddLocationViewModelFactory
import com.mobiquity.weatherapp.db.WeatherDao
import com.mobiquity.weatherapp.db.WeatherDatabase
import com.mobiquity.weatherapp.details.LocationDetailsViewModelFactory
import com.mobiquity.weatherapp.locations.LocationRepository
import com.mobiquity.weatherapp.locations.LocationsListViewModelFactory
import com.mobiquity.weatherapp.retrofit.ApiClient
import com.mobiquity.weatherapp.retrofit.ApiInterface
import java.text.SimpleDateFormat

object Dependencies {

    private fun getWeatherDatabase(context: Context): WeatherDatabase {
        return WeatherDatabase.getDatabase(context)
    }

    private fun getWeatherTestDatabase(context: Context): WeatherDatabase {
        return Room.inMemoryDatabaseBuilder(context, WeatherDatabase::class.java).allowMainThreadQueries().build()
    }

    private fun getWeatherDao(weatherDatabase: WeatherDatabase): WeatherDao {
        return weatherDatabase.weatherDao()
    }

    fun getLocationRepository(context: Context, isTesting : Boolean = false): LocationRepository {
        if(isTesting){
            return LocationRepository(getWeatherDao(getWeatherTestDatabase(context.applicationContext)), getRetrofit())
        }
        else{
            return LocationRepository(getWeatherDao(getWeatherDatabase(context.applicationContext)), getRetrofit())
        }
    }

    fun getLocationsListViewModelFactory(application: Application): LocationsListViewModelFactory {
        return LocationsListViewModelFactory(
            application,
            getLocationRepository(application.baseContext)
        )
    }

    fun getAddLocationViewModelFactory(application: Application): AddLocationViewModelFactory {
        return AddLocationViewModelFactory(
            application,
            getLocationRepository(application.baseContext)
        )
    }

    private fun getRetrofit(): ApiInterface {
        return ApiClient().makeRetrofitService()
    }

    fun getLocationDetailsViewModelFactory(application: Application): LocationDetailsViewModelFactory {
        return LocationDetailsViewModelFactory(
            application,
            getLocationRepository(application.baseContext)
        )
    }

    fun provideDateTextSdfTime() : SimpleDateFormat {
        return SimpleDateFormat("yyyy-MM-dd HH:mm")
    }

    fun provideDateSdfTime() : SimpleDateFormat {
        return SimpleDateFormat("yyyy-MM-dd")
    }
}