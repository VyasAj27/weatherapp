package com.mobiquity.weatherapp.utils

object Constants {

    const val APP_ID_OPEN_WEATHER = /*"7c76a39f534f7040869aacca42929fcb"*//*"a86a0ece70cabbffd389cbcdbbafcb3f"*/"fae7190d7e6433ec3a45285ffcf55c86"
    const val BASE_URL = "https://api.openweathermap.org/data/2.5/"
    const val GET_WEATHER = "${BASE_URL}weather"
    const val GET_WEATHER_FORECAST_AHEAD = "${BASE_URL}forecast"

    const val LOCATION_BUNDLE = "location"
}