package com.mobiquity.weatherapp.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import com.mobiquity.weatherapp.getOrAwaitValue
import com.mobiquity.weatherapp.models.Location
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * This test is used to test DAO in fake Database object. We test all the methods of the dao.
 */
@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class WeatherDaoTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: WeatherDatabase
    private lateinit var dao: WeatherDao

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            WeatherDatabase::class.java
        ).allowMainThreadQueries().build()

        dao = database.weatherDao()
    }

    @After
    fun teardown() {
        database.close()
    }

    @Test
    fun insertLocation() = runBlockingTest {
        val location = Location(name = "Vadodara")
        dao.insertLocation(location)

        val listOfLocations = dao.fetchLocations().getOrAwaitValue()

        //Lets check if the updated list has the location that we have added. we check here for
        //name property which we have set. Checking direct object will cause test to fail as we have
        //set room to set id (primary key) which is autocreated. So, check for the property not direct object.
        assertThat(listOfLocations[0].name).isEqualTo(location.name)
    }

    @Test
    fun deleteLocation() = runBlockingTest {

        val location = Location(name = "Vadodara")
        dao.insertLocation(location)

        //get the location from db. so we can use it to test delete functionality
        val listOfLocations = dao.fetchLocations().getOrAwaitValue()
        val gotLocation = listOfLocations[0]

        dao.deleteLocation(gotLocation.id)

        //Check if its removed or not.
        assertThat(dao.fetchLocations().getOrAwaitValue()).doesNotContain(gotLocation)
    }

    @Test
    fun deleteLocations() = runBlockingTest {
        val location1 = Location(name = "Vadodara")
        dao.insertLocation(location1)
        val location2 = Location(name = "Surat")
        dao.insertLocation(location2)

        //Now delete all.
        dao.deleteLocations()

        //Check if its clear or not.
        assertThat(dao.fetchLocations().getOrAwaitValue()).isEmpty()
    }
}